import React from 'react';
import ReactDOM from 'react-dom';
import TodoApp from './TodoApp';
import 'semantic-ui-css/semantic.min.css';
import "./index.css";

var destination = document.querySelector("#container");

ReactDOM.render(
    <div>
        <TodoApp/>
    </div>
, destination);

