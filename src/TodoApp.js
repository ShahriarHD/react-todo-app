import React, { Component } from 'react';
import axios from 'axios'; //HTTP Client
import Task from './Task';
import { Divider, Header, Icon, Input, List, Grid } from 'semantic-ui-react'

//import 'TodoApp.css';
//axios.defaults.baseURL = 'http://localhost:8000/'; // Development enviroment
axios.defaults.baseURL = 'http://139.59.169.113:8000/'; // Production enviroment

export default class TodoApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: [],
            newTask: ''
        }
    }

    componentWillMount() {
        this.getTasks();
    }

    deleteTask = (taskId) => {
        const tasks = this.state.tasks.filter(task => {
            return (task.id !== taskId);
        });
        this.setState({ tasks });
    }

    updateTask = (taskId, text) => {
        const tasks = this.state.tasks.map(task => {
            if (task.id === taskId) {
                task.text = text;
            }
            return task;
        });
        this.setState({ tasks });
    }

    getTasks = () => {

        axios.get('/todos')
            .then(response => {
                const _tasks = response.data;
                const tasks = _tasks.map(_task => {
                    let task = {};
                    task.text = _task.text;
                    task.id = _task._id;
                    return task;
                });
                this.setState({ tasks });
            })
            .catch(error => {
                console.error(error);
            });
    }

    createTask = () => {
        axios.post('/todos', { text: this._inputElement.inputRef.value })
            .then(response => {
                this._inputElement.inputRef.value = '';

                const _task = response.data;
                let task = {};
                task.text = _task.text;
                task.id = _task._id;

                const tasks = this.state.tasks;
                tasks.unshift(task);
                this.setState({ tasks });
            })
            .catch(error => {
                console.error(error);
            });
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.createTask();
        }
    }

    render() {
        const todoList = this.state.tasks.map((task, index) => {
            return <Task text={task.text}
                taskId={task.id}
                deleteTask={this.deleteTask}
                updateTask={this.updateTask}
                key={task.id}
            />
        });
        return (
            <div className="todo-app-main">
                <Grid centered columns={3}
                        >
                    <Grid.Row centered columns={8}>
                        <Grid.Column width={8}>
                            <Header as="h2" icon>
                                <Icon name="checkmark box" />
                                Simple Todo
                            <Header.Subheader>
                                    a simple todo application <br />implemented in React, NodeJS, Couchbase.
                            </Header.Subheader>
                            </Header>
                        </Grid.Column>
                    </Grid.Row>

                    <Divider />
                    <Grid.Row centered columns={8}>
                        <Grid.Column width={8}>
                            <div className="todo-app-container">
                                <Input size='huge' placeholder="enter task"
                                    action={{ onClick: this.createTask, icon: 'add' }}
                                    onKeyPress={this.handleKeyPress}
                                    ref={a => this._inputElement = a} />
                                <List relaxed animated>
                                    {todoList}
                                </List>
                            </div>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        );
    }
}