import React, { Component } from 'react';
import axios from 'axios'; // HTTP Client
import { Input, Icon, Label, List, Modal, Header, Button, Divider } from 'semantic-ui-react'

//axios.defaults.baseURL = 'http://localhost:8000/'; // Development enviroment
axios.defaults.baseURL = 'http://139.59.169.113:8000/'; // Production enviroment

export default class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: props.text,
            editMode: false,
        };
    }

    deleteTask = () => {
        axios.delete(`/todos/${this.props.taskId}`)
            .then(() => {
                this.props.deleteTask(this.props.taskId);
            })
            .catch(error => {
                console.error(error);
            });
    }

    updateTask = () => {
        axios.put(`/todos/${this.props.taskId}`, { text: this._inputElement.inputRef.value })
            .then(() => {
                this.setState({
                    text: this._inputElement.inputRef.value,
                });
                this.props.updateTask(this.props.taskId, this._inputElement.inputRef.value);
                this.toggleEditMode();
            })
            .catch(error => {
                console.error(error);
            });
    }

    toggleEditMode = () => {
        this.setState({
            editMode: !this.state.editMode,
        });
    }

    render() {
        return (
            <List.Item>
                <Label size="huge" className="label-box" onClick={this.toggleEditMode}>
                    {this.state.text}
                </Label>
                <Modal open={this.state.editMode}
                       size="small"
                       onClose={this.toggleEditMode}
                       basic
                       >
                    <Header content="Edit Task" />
                    <Modal.Content>
                        <Input defaultValue={this.state.text}
                               placeholder="Update Task"
                               ref={(a) => this._inputElement = a}
                               fluid
                               transparent
                               inverted/>
                        <Divider fitted/>
                    </Modal.Content>
                    <Modal.Actions>
                        <Button basic color='red' onClick={this.toggleEditMode} inverted>
                            <Icon name='remove' /> Cancel
                        </Button>
                        <Button color='red' onClick={this.deleteTask} inverted>
                            <Icon name='trash' /> Delete
                        </Button>
                        <Button color='green' onClick={this.updateTask} inverted>
                            <Icon name='checkmark' /> Update
                        </Button>
                    </Modal.Actions>
                </Modal>
            </List.Item>
        );
    }
}